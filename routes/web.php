<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('trainer','TrainerController');
// Route::resource('pokemon', 'PokemonController');
Route::get('trainer/{trainer}/pokemon', 'PokemonController@index');
Route::post('trainer/{trainer}/pokemon','PokemonController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
