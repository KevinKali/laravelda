<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainer;
use App\Http\Requests\StoreTrainerRequest;

class TrainerController extends Controller
{
    // public function index()
    // {
    //     $trainers = Trainer::all();
    //     // return "Hello world at trainer";
    //     // return $trainers;
    //     return view('trainers.index', ['trainers'=>$trainers]);
    // }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $trainers = Trainer::all();
        return view('trainers.index', ['trainers'=>$trainers]);
    }

    public function create()
    {
        return view('trainers.create');
    }

    public function store(StoreTrainerRequest $request)
    {
        $trainer = new Trainer();
        $name = "";
        
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path()."/images/",$name);
        }
        $trainer->name = $request->input('name');
        $trainer->slug = $request->input('name');
        $trainer->avatar = $name;
        $trainer->save();

        return redirect()->action('TrainerController@show', compact('trainer'));
    }
    // public function store(Request $request)
    // {

    //     // $validateData = $request->validate([
    //     //     'name'=> 'required|max: 10',
    //     //     'avatar'=> 'required|image'
    //     // ]);

    //     $trainer = new Trainer();
    //     $name = "";
        
    //     if($request->hasFile('avatar')){
    //         $file = $request->file('avatar');
    //         $name = time().$file->getClientOriginalName();
    //         $file->move(public_path()."/images/",$name);
    //     }
    //     $trainer->name = $request->input('name');
    //     $trainer->slug = $request->input('name');
    //     $trainer->avatar = $name;
    //     $trainer->save();

    //     return 'Saved';
    //     // return $request->all();
    //     // return $request->input('name');
    // }

    // public function show($id)
    // {
    //     $trainer = Trainer::findOrFail($id);
    //     return view('trainers.show', ['trainer'=>$trainer]);
    // }

    // ************************* haciendo uso de binding eloquent
    // public function show(Trainer $trainer){
    //     return view('trainers.show', ['trainer'=>$trainer]);
    // }

    // public function show($slug){
    //     $trainer = Trainer::where('slug','=',$slug)->firstOrFail();

    //     return view('trainers.show', ['trainer'=>$trainer]);
    // }

    // custom binding route model    (manda a llamar el metodo getRouteKey escrito en el modelo Trainer)
    public function show(Trainer $trainer){
        return view('trainers.show', compact('trainer'));
    }

    public function edit(Trainer $trainer){
        return view('trainers.edit', compact('trainer'));
    }

    public function update(Request $request, Trainer $trainer){
        $trainer->fill($request->except('avatar'));

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $trainer->avatar = $name;
            $file->move(public_path()."/images/",$name);
        }
        $trainer->slug =$request->input('slug');
        $trainer->save();

        // return $request->all();
        // return redirect()->action('TrainerController@show', compact('trainer'));
        return redirect()->route('trainer.show', [$trainer])
                        ->with('status','Entrenador Actualizado Correctamente');
    }

    // public function edit($id)
    // {
    //     //
    // }

    // public function update(Request $request, $id)
    // {
    //     //
    // }

    public function destroy(Trainer $trainer)
    {
        $file_path = public_path(). '/images/'. $trainer->avatar;
        \File::delete($file_path);
        $trainer->delete();
        // return redirect()->action('TrainerController@index');
        return redirect()->route('trainer.index');
    }
    // public function destroy($id)
    // {
        
    // }
}
