<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainer;
use App\Pokemon;

class PokemonController extends Controller
{
    public function index(Trainer $trainer, Request $request){
        if($request->ajax()){
            // $pokemons = Pokemon::all();
            $pokemons = $trainer->pokemons;
            return response()->json($pokemons, 200);
            // return response()->json([
            //     ['id'=>1, 'name'=>"Pikachu"],
            //     ['id'=>2, 'name'=>"Charizard"],
            //     ['id'=>3, 'name'=>"Squirtle"]
            // ],200);
        }
        return view('pokemon.index');
    }

    public function store(Trainer $trainer, Request $request){
        if($request->ajax()){
            $pokemon = new Pokemon();
            $pokemon->name = $request->input('name');
            $pokemon->picture = $request->input('picture');
            $pokemon->trainer()->associate($trainer)->save();

            return response()->json([
                "trainer" => $trainer,
                "message" => "Pokemon Created good"
                // "pokemon" => $pokemon
            ],200);
        }
        return "error";
    }
}
