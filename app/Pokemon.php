<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    public function Trainer(){
        return $this->belongsTo('App\Trainer');
    }
}
