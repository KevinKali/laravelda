<?php

use Illuminate\Database\Seeder;
use App\Trainer;
use App\Pokemon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        self::seedUser();
        // $this->call(RoleTableSeeder::class);
        // $this->call(UserTableSeeder::class);
    }

    private function seedUser(){
        DB::table('trainers')->delete();

        // $trainer = new Trainer;
        // $trainer->name = 'kevin';
        // $trainer->slug = 'kevin';
        // $trainer->avatar = '1583814852Anotación 2020-03-06 195628.jpg';
        // $trainer->save();

        // $trainer = new Trainer;
        // $trainer->name = 'Josue';
        // $trainer->slug = 'Josue';
        // $trainer->avatar = '1583814313540338.jpg';
        // $trainer->save();

        $pokemon = new Pokemon();
        $pokemon->name = "asdawds";
        $pokemon->picture = "awdasdawdawdas";
        $pokemon->save();
    }
}
