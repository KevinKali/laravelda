<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    
    public function run()
    {
        $role_user = Role::where('name','user')->first();
        $role_admin = Role::where('name','admin')->first();

        $user = new User();
        $user->name = "User";
        $user->email = "tuani2@yeah.com";
        $user->password = bcrypt('tuani');
        $user->save();
        // para que se relacionen 
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = "admin";
        $user->email = "tuani3@yeah.com";
        $user->password = bcrypt('tuani2');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
