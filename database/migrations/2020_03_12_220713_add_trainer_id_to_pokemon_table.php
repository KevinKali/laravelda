<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrainerIdToPokemonTable extends Migration
{
    public function up()
    {
        Schema::table('pokemon', function (Blueprint $table) {
            $table->integer('trainer_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('pokemon', function (Blueprint $table) {
            $table->dropColumn('trainer_id');
        });
    }
}
