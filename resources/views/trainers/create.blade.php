@extends('layouts.app')

@section('title','Trainer Create')

@section('content')
    {{-- @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    @endif --}}
    @include('common.errors')

    <form action="/trainer" method="POST" enctype="multipart/form-data">
        @csrf
        <label for="">Nombre: </label>
        <input type="text" name="name" id="name">
        <br>
        <label for="">File: </label>
        <input type="file" name="avatar" id="avatar">

        <input type="submit" value="Create">
    </form>
@endsection