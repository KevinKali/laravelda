@extends('layouts.app')
@section('title','mostrando')

@section('content')
{{-- @if (session('status'))
    <h1>{{session('status')}}</h1>
@endif --}}
@include('common.status')

<h2>{{$trainer->name}}</h2>
<h4>{{$trainer->slug}}</h4>
<img src="/images/{{$trainer->avatar}}" alt="" width="100px">
<a href="/trainer/{{$trainer->slug}}/edit">Editar</a>

<form action="/trainer/{{$trainer->slug}}" method="POST">
    @method('DELETE')
    @csrf
    <input type="submit" value="Borrar">
</form>

<add-pokemon-btn></add-pokemon-btn>
<create-form-pokemon></create-form-pokemon>
{{-- <spinner></spinner> --}}
<pokemon-component></pokemon-component>
@endsection