@extends('layouts.app')

@section('title','Trainer Edit')

@section('content')
    <form action="/trainer/{{$trainer->slug}}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        @include('partials.form');
    </form>
@endsection

    
