@extends('layouts.app')

@section('title', 'index')

@section('content')

<div class="container-fluid content-row">
    <div class="row">

    @foreach ($trainers as $item)
        <div class="col-lg-4 d-flex align-items-stretch" style="justify-content: center;">
    <div class="card" style="align-self: center; align-items: center;">
        <img src="images/{{$item->avatar}}" class="card-img-top rounded" alt="..." style="width:100px; height:100px; border-radius: 50% !important;">
        <div class="card-body">
            <h5 class="card-title">{{$item->name}}</h5>
            <a href="/trainer/{{$item->slug}}" class="btn btn-primary">ver mas...</a>
        </div>
    </div>
</div>
    @endforeach

    
</div>
</div>
@endsection