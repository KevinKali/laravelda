@extends('layouts.app')

@section('content')
<div class="container-fluid content-row">
    <add-pokemon-btn></add-pokemon-btn>
    <pokemon-component></pokemon-component>
    <create-form-pokemon></create-form-pokemon>
</div>
@endsection